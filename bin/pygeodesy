#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import pygeodesy as pg
import argparse
import sys

def parse():
    parser = argparse.ArgumentParser(description="""
    pygeodesy - Python module for reading and processing geodetic time series data.""")
   
    # Define subparsers for each module 
    subparsers = parser.add_subparsers(help='Module options', dest='module')

    # Options for makedb
    dbparser = subparsers.add_parser('makedb', help='Create database from time series.')
    dbparser.add_argument('-cfg', action='store', default='data.cfg',
        help='Database configuration file. Default: data.cfg.')
    dbparser.add_argument('-v', '-V', action='store_true', dest='verbose',
        help='Verbose mode.')

    # Options for subnet
    subnet = subparsers.add_parser('subnet', help='Extract sub-network from database.')
    subnet.add_argument('-cfg', action='store', default='data.cfg',
        help='Subset network configuration file. Default: data.cfg.')

    # Options for clean
    clean = subparsers.add_parser('clean', 
        help='Use header data from files to remove offsets from data.')
    clean.add_argument('-cfg', action='store', default='data.cfg',
        help='Cleaning configuration file. Default: data.cfg.')

    # Options for cme
    cme = subparsers.add_parser('cme', 
        help='Perform common mode estimation on data.')
    cme.add_argument('-cfg', action='store', default='data.cfg',
        help='CME configuration file. Default: data.cfg.')

    # Options for filter
    filter = subparsers.add_parser('filter', 
        help='Filter data to a specified window size.')
    filter.add_argument('-cfg', action='store', default='data.cfg',
        help='Filter configuration file. Default: data.cfg.')

    # Options for detrend
    detrend = subparsers.add_parser('detrend', help='Detrend time series in database.')
    detrend.add_argument('-cfg', action='store', default='data.cfg',
        help='Detrend configuration file. Default: data.cfg.')

    # Options for modelfit
    detrend = subparsers.add_parser('modelfit', help='Fit model to time series in database.')
    detrend.add_argument('-cfg', action='store', default='data.cfg',
        help='Modelfit configuration file. Default: data.cfg.')

    # Options for elasticnet
    elasticnet = subparsers.add_parser('elasticnet', 
        help='Perform elastic net decomposition on database.')
    elasticnet.add_argument('-cfg', action='store', default='data.cfg',
        help='Elastic Net configuration file. Default: data.cfg.')

    # Options for plotting
    plot = subparsers.add_parser('plot', help='Plot time series for specified stations.')
    plot.add_argument('-cfg', action='store', default='plot.cfg',
        help='Plotting configuration file. Default: plot.cfg.')
    
    # Options for network map
    netmap = subparsers.add_parser('netmap', help='Make interactive map of network.')
    netmap.add_argument('-cfg', action='store', default='plot.cfg',
        help='Plotting configuration file. Default: plot.cfg.')

    # Options for velocity map
    velmap = subparsers.add_parser('velmap', help='Make velocity map of network.')
    velmap.add_argument('-cfg', action='store', default='plot.cfg',
        help='Plotting configuration file. Default: plot.cfg.')


    return parser.parse_args()
    

if __name__ == '__main__':

    # Get the command line options
    inputs = parse()

    if inputs.module in ['makedb', 'subnet', 'clean', 'cme', 'filter', 'detrend', 
        'modelfit', 'elasticnet', 'plot', 'netmap', 'velmap']:

        # Parse the configuration file for the given module
        config = pg.Configuration(inputs.cfg, inputs.module)

        # Run the appropriate module
        if inputs.module == 'makedb':
            pg.db.makedb(config())

        elif inputs.module == 'subnet':
            pg.db.subnet(config())

        elif inputs.module == 'clean':
            pg.network.clean(config())

        elif inputs.module == 'filter':
            pg.network.filter(config())

        elif inputs.module == 'cme':
            pg.network.cme(config())

        elif inputs.module == 'modelfit':
            pg.network.modelfit(config())

        elif inputs.module == 'detrend':
            pg.network.detrend(config())

        elif inputs.module == 'elasticnet':
            pg.network.elasticnet(config())

        elif inputs.module == 'plot':
            pg.view.plot(config())

        elif inputs.module == 'netmap':
            pg.view.netmap(config())

        elif inputs.module == 'velmap':
            pg.view.velmap(config())

    else:
        print('Module', inputs.module, 'is not supported')

        
# end of file
